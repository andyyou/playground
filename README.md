Try to use `react` to build a website. 

## Requirements

* Using `create-react-app` or `webpack` to initialize the project.
* Switch pages with `react-router-dom`.
* Support internationalization with `react-intl`. Two languages support at least.
* Styling the site with `styled-components`.
* (Optional) Using `antd` design framework or bootstrap.
* (Optional) In addition to CSS-in-JS, if you extends CSS solution, please support naming convention such as BEM.

