import React, {
  useState,
} from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
} from 'react-router-dom';
import {
  IntlProvider,
  FormattedMessage,
} from 'react-intl';


import Home from './components/Home';
import About from './components/About';

import en from './locale/en.json';
import zhTW from './locale/zh-TW.json';

function App() {
  const [locale, setLocale] = useState('en');
  const messages = {
    en,
    "zh-TW": zhTW,
  };
  return (
    <IntlProvider locale="zh-TW" defaultLocale="en" messages={messages[locale]}>
      <Router>
        <div>
          <Link to="/">
            <FormattedMessage id="home" />
          </Link>
          {' '}
          <Link to="/about">
            <FormattedMessage id="about" />
          </Link>
        </div>

        <Switch>
          <Route path="/" component={Home} exact={true} />
          <Route path="/about" component={About} />
        </Switch>
        <footer>
          <a
            href="#"
            onClick={(e) => {
              e.preventDefault();
              setLocale('en');
            }}
          >
            English
          </a>
          <a
            href="#"
            onClick={(e) => {
              e.preventDefault();
              setLocale('zh-TW');
            }}
          >
            中文
          </a>
        </footer>
      </Router>
    </IntlProvider>
  );
}

export default App;
